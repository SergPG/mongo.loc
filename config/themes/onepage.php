<?php

return [

    //
        // 'name' => 'onepage',
        // 'theme' => 'onepage',


    'site' => [ 
        'title' => 'Onepage Theme',
        'description' => 'Onepage Theme',
        'logo' => [ 'logo-img-main'=>'https://picsum.photos/150/30?gravity=center',
                  'logo-img-active' => 'https://picsum.photos/150/30?gravity=center',
                  'footer-logo' => 'https://picsum.photos/150/30?gravity=center',],
     ],  



  
    //   Field of relationships            'id_onepage', 
    //   Field of number position          'position', 
    'sections' => [ 
            'section1' => ['alias'	=> 'header',
                        'view_f' => 'header',				
                        'show'=> yes,],

            'section2' => ['alias' => 'slides',  
                        'view_f' => 'slides',          
                        'show'=> yes,],
       
            'section3' => ['alias' => 'about',
                        'view_f' => 'section_about',         
                        'show'=> yes,],

            'section4' => ['title' => 'Latest Products',
                           'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna aliqua enim minim veniam exercitation' ,
                           'alias' => 'products', 
                           'view_f' => 'section_products',        
                           'show'=> yes,], 

            'section5' => ['alias' => 'pricing', 
                        'view_f' => 'section_pricing',     
                    	  'show'=> yes,],
     
            'section6' => ['title' => 'Work',
                       'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna aliqua enim minim veniam exercitation' ,
                       'alias' => 'work', 
                       'view_f' => 'section_work',     
                       'show'=> yes,], 
           
            'section7' => ['title' => 'Services',
                         'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna aliqua enim minim veniam exercitation' ,
                         'alias' => 'services',
                         'view_f' => 'section_services',      
                       	 'show'=> yes,], 
   
            'section8' => ['alias' => 'contact', 
                          'view_f' => 'section_contact',     
                      	  'show'=> yes,],
         
            'section9' => ['alias' => 'footer', 
                         'view_f' => 'footer',     
                    	   'show'=> yes,],
        ], 
  
    // Lists Site
    //   Field of relationships            'id_onepage',     

  'menuLists' => [
       'list1' => ['menu_top_onepage' => ['Home' => '#body',
                                         'About'=> '#about',
                                         'Products' => '#products',
                                         'Pricing' => '#pricing',
                                         'Work' => '#work',
                                         'Services' => '#services',
                                          'Contact' => '#contact',],
                

                  'menu_footer_onepage' => ['Home' => '#body',
                                           'About'=> '#about',
                                           'Work' => '#work',
                                           'Contact' => '#contact',],
          

                  'menu_social_onepage' => ['Twitter' => '#',
                                            'Facebook'=> '#',
                                            'Instagram' => '#',
                                            'YouTube' => '#',],
  
                  'menu_contracts_onepage' => ['Subscribe to Our Newsletter' => '#',
                                            'Privacy Policy'=> '#',
                                            'Terms &amp; Conditions' => '#',],
                  
               ],
    ],





   	// Slides Top
    //   Field of relationships            'id_onepage',     
   	'slides' =>	[
		'slide1'=> ['image' => 'https://picsum.photos/1920/1080?image=1005' ,
				   'title' => 'Hi-Tech 1 Design' , 
				   'description' => 'Lorem ipsum dolor amet consectetur adipiscing dolore magna aliqua <br/> enim minim estudiat veniam siad venumus dolore' ,
				   'url' => '#' , 
				  ],
		'slide2'=> ['image' => 'https://picsum.photos/1920/1080?image=405' ,
				   'title' => 'Hi-Tech 2 Design' , 
				   'description' => 'Lorem ipsum dolor amet consectetur adipiscing dolore magna aliqua <br/> enim minim estudiat veniam siad venumus dolore' ,
				   'url' => '#' , 
				  ],
		'slide3'=> ['image' => 'https://picsum.photos/1920/1080?image=320' ,
				   'title' => 'Hi-Tech 3 Design' , 
				   'description' => 'Lorem ipsum dolor amet consectetur adipiscing dolore magna aliqua <br/> enim minim estudiat veniam siad venumus dolore' ,
				   'url' => '#' , 
				  ],
  	 	],			         			 
   					         			
  // Notes - mini post  (section About)
  //   Field of relationships            'id_onepage', 
  'notes' =>[
    'note1' => ['image' => 'https://picsum.photos/500/500?image=147' ,
              'title' => 'Art Of Coding' ,
              'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna ut consequat siad esqudiat dolor' ,],

    'note2' => ['image' => 'https://picsum.photos/500/500?image=900' ,
              'title' => 'Clean Design' ,
              'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna ut consequat siad esqudiat dolor' ,],

    'note3' => ['image' => 'https://picsum.photos/500/500?image=65' ,
              'title' => 'Amazing Support' ,
              'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna ut consequat siad esqudiat dolor',],
     ],
   
   // OnePosts - Posts in sections
   //   Field of relationships            'id_onepage',  
     'oneposts' => [
        'onepost1' => ['image' => null ,
                     'title' => 'Why Choose Us?' ,
                     'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                     'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                         Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                     'alias' => 'about_post', ], //alias section
        ],

   // Accordions 
   //   Field of relationships            'id_onepage',  
     'accordions' => [
        'accordion1' => ['title' => 'Exceptional Frontend Framework' ,
                     'text' => 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.',], 

        'accordion2' => ['title' => 'Modern Design Trends' ,
                     'text' => 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.',], 

        'accordion3' => ['title' => 'Beatifully Crafted Code' ,
                     'text' => 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.',], 
        ],

   // Products  
   //   Field of relationships            'id_onepage',  
    'products' =>[
      'product1' => ['image' => 'https://picsum.photos/970/647?image=802' ,
                    'title' => 'Workspace' ,
                    'category' => 'Management' ,
                    'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.', ],
    
       'product2' => ['image' => 'https://picsum.photos/970/647?image=4' ,
                    'title' => 'Minimalism' ,
                    'category' => 'Developmeny' ,
                    'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.', ],
  
        'product3' => ['image' => 'https://picsum.photos/970/647?image=8' ,
                    'title' => 'Cleant Style' ,
                    'category' => 'Design' ,
                    'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magna ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.', ],
       ],             

   // Pricing  
   //   Field of relationships            'id_onepage',  
    'pricings' =>[
        'pricing1' => ['icon' => 'icon-chemistry' ,
                    'title' => 'Starter Kit ' ,
                    'price' => '49',
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor',
                    'data_sheet' => ['Basic Features','Up to 5 products', '50 Users Panels',],
                    'active' => false,],

        'pricing2' => ['icon' => 'icon-badge' ,
                    'title' => 'Professional ' ,
                    'price' => '149',
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor',
                    'data_sheet' => ['Basic Features','Up to 100 products', '100 Users Panels',],
                    'active' => true,],

        'pricing3' => ['icon' => 'icon-shield' ,
                    'title' => 'Advanced ' ,
                    'price' => '249',
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor',
                    'data_sheet' => ['Extended Features','Unlimited products', 'Unlimited Users Panels',],
                    'active' => false,],
         ],


   // Work  
   //   Field of relationships            'id_onepage',  
  // imege 800 x 400
  // imege 397 x 415
  // imege 397 x 300

    'works' => [     
        'work1' => ['image' => 'https://picsum.photos/800/400?image=999' , 
                'title' => 'Art Of Coding #1' ,
                'category' => 'Clean &amp; Minimalistic Design' ,
                'text' => ' <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>', 
                'tegs' => ['Design' => '#', 'Coding' => '#', 'Portfolio'=> '#',], 
                'workgroup' => ['Project Leader' => 'John Doe',
                               'Designer' => 'Alisa Keys',
                               'Developer'=> 'Mark Doe',
                               'Customer'=> 'Keenthemes',], 
              ],
        
        'work2' => ['image' => 'https://picsum.photos/397/415?image=408',
                'title' => 'Art Of Coding #2' ,
                'category' => 'Clean &amp; Minimalistic Design' ,
                'text' => ' <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>', 
                'tegs' => ['Design' => '#', 'Coding' => '#', 'Portfolio'=> '#',], 
                'workgroup' => ['Project Leader' => 'John Doe',
                               'Designer' => 'Alisa Keys',
                               'Developer'=> 'Mark Doe',
                               'Customer'=> 'Keenthemes',], 
                 ],

        'work3' => ['image' => 'https://picsum.photos/397/300?image=740', 
                'title' => 'Art Of Coding #3' ,
                'category' => 'Clean &amp; Minimalistic Design' ,
                'text' => ' <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>', 
                'tegs' => ['Design' => '#', 'Coding' => '#', 'Portfolio'=> '#',], 
                'workgroup' => ['Project Leader' => 'John Doe',
                               'Designer' => 'Alisa Keys',
                               'Developer'=> 'Mark Doe',
                               'Customer'=> 'Keenthemes',], 
              ],
   
        'work4' => ['image' => 'https://picsum.photos/397/300?image=998',
                'title' => 'Art Of Coding #4' ,
                'category' => 'Clean &amp; Minimalistic Design' ,
                'text' => ' <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>', 
                'tegs' => ['Design' => '#', 'Coding' => '#', 'Portfolio'=> '#',], 
                'workgroup' => ['Project Leader' => 'John Doe',
                               'Designer' => 'Alisa Keys',
                               'Developer'=> 'Mark Doe',
                               'Customer'=> 'Keenthemes',], 
              ],

         'work5' => ['image' => 'https://picsum.photos/397/300?image=77',
                'title' => 'Art Of Coding #5' ,
                'category' => 'Clean &amp; Minimalistic Design' ,
                'text' => ' <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>', 
                'tegs' => ['Design' => '#', 'Coding' => '#', 'Portfolio'=> '#',], 
                'workgroup' => ['Project Leader' => 'John Doe',
                               'Designer' => 'Alisa Keys',
                               'Developer'=> 'Mark Doe',
                               'Customer'=> 'Keenthemes',], 
              ],
          ], 
      
  
  //   Field of relationships            'id_onepage',  
       'swipings' =>[
          'card1' => ['alias' => 'clients', 
                    'clients' => [
                      'Client Name 1' => 'https://picsum.photos/200/100?image=437' ,
                      'Client Name 2' => 'https://picsum.photos/200/100?image=164' ,
                      'Client Name 3' => 'https://picsum.photos/200/100?image=281' ,
                      'Client Name 4' => 'https://picsum.photos/200/100?image=299' ,
                      'Client Name 5' => 'https://picsum.photos/200/100?image=369' ,
                      'Client Name 6' => 'https://picsum.photos/200/100?image=419' ,
                                  ],
                    ],
                ],


   // Services  
   //   Field of relationships            'id_onepage',  
    'services' =>[
        'service1' => ['icon' => 'icon-chemistry' ,
                    'title' => 'Art Of Coding' ,
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.',
                    'active' => false, ],
    
        'service2' => ['icon' => 'icon-screen-tablet' ,
                    'title' => 'Responsive Design' ,
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.',
                    'active' => true,],
    
        'service3' => ['icon' => 'icon-badge' ,
                    'title' => 'Feature Reach' ,
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.', 
                    'active' => false,],
     
         'service4' => ['icon' => 'icon-notebook' ,
                    'title' => 'Useful Documentation' ,
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.', 
                    'active' => false,],
    
        'service5' => ['icon' => 'icon-speedometer' ,
                    'title' => 'Fast Delivery' ,
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.', 
                    'active' => false,],
    
         'service6' => ['icon' => 'icon-badge' ,
                    'title' => 'Free Plugins' ,
                    'description' => 'Lorem ipsum dolor amet consectetur ut consequat siad esqudiat dolor' ,
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor 
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.', 
                    'active' => false,],
       ],            

    // Contact Lists  
   //   Field of relationships            'id_onepage',  
    'contacts' =>[
        'contact1' =>[ 'city' => 'New Yor',
                          'office ' => 'Head Office', 
                          'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor' ,
                          'contact_list' => ['icon-call-out' => ['44 77 3456 7890','11 77 3100 7890',],
                                            'icon-envelope' => ['operation@AitOnepage.com',],
                                            ],
                          ],
        'contact2' =>[ 'city' => 'London',
                          'office ' => 'Operation', 
                          'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor' ,
                           'contact_list' => ['icon-call-out' => ['44 77 3456 7890',],
                                            'icon-envelope' => ['operation@AitOnepage.com','opeAS105ration@AitOnepage.com',],
                                            ],
                          ],
        'contact3' =>[ 'city' => 'Singapore',
                          'office ' => 'Finance', 
                          'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor' ,
                         'contact_list' => ['icon-call-out' => ['44 77 3456 7890',],
                                            'icon-envelope' => ['operation@AitOnepage.com',],
                                            ],
                          ],
    ],

   

];  // END
