<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('pages/{theme}', 'Frontend\PageController@index')->name('pages.show');


// Admin
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	//admin
	Route::get('/','Backend\DashboardController@index')->name('admin.dashboard');
	
	//Themes 
	 Route::resource('themes','Backend\ThemeController');
	
	//Onepages 
	 Route::resource('onepages','Backend\OnepageController');
	

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
