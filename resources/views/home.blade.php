@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                   
                  

                  <div class="masonry-grid-item col-xs-12 col-sm-6 col-md-4">
                        <div class="margin-t-60 margin-b-60">
                            <h2>Amazing Support</h2>
                            <p> {{ $url }} </p>
                        </div>
                        <img class="full-width img-responsive wow fadeInUp" src="{{ asset($url) }}" alt="Portfolio Image" data-wow-duration=".3" data-wow-delay=".4s">
                    </div>


                      <example></example>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
