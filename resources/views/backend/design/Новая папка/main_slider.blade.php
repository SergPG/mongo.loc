<!-- slider -->


<div class="slider">

@if(isset($sliders) && is_object($sliders))
					
	<!-- carousel__element owl-carousel -->
	<div class="carousel__element owl-carousel" data-options='{"items":1,"loop":true,"dots":false,"nav":false,"margin":0, "autoplay": true, "autoplayTimeout": 9000}'>

		 @foreach($sliders as $slider)
		
					<div class="slider__item" style="background-image: url('{{ asset('storage/img/'.$slider->image) }}');">
						<div class="md-tb">
								<div class="md-tb__cell">
									<div class="slider__content">
										<div class="container">
											<h2>{{$slider->title}}</h2>
											<p>{{$slider->description}}</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
			@endforeach			

	</div><!-- End / carousel__element owl-carousel -->

	@endif				
</div><!-- End / slider -->
