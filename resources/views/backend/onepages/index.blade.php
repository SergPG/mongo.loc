@extends('backend.layouts.backend')

@section('content')    

   <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">OnePages</li>
      </ol>



      


      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table OnePages

          <div class="btn-group float-right" role="group" aria-label="Basic example">
               <a id="new_theme"class="btn btn-outline-success" href="{{ route('onepages.create') }}" role="button"><i class="fa fa-lg fa-plus"  title="Add Theme"></i></a>
            
          </div>

        </div>


        <div class="card-body">
 @if(isset($onepages) && is_object($onepages))    

          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Title</th>
                  <th>Descriptipn</th>
                  <th>Theme</th>
                  
                  <th>Actions</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Title</th>
                  <th>Descriptipn</th>
                  <th>Theme</th>
                  
                  <th>Actions</th>
                </tr>
              </tfoot>
              <tbody>


 @foreach($onepages as $onepage)
                <tr>
                  <td>{{ $onepage->name }}</td>
                  <td>{{ $onepage->title }}</td>
                  <td>{{ $onepage->descriptipn }}</td>
                  <td>{{ $onepage->theme }}</td>
                  
                  
          <td>
            <div class="btn-group" role="group" aria-label="Basic example">
              
              <a id="new_onepage"class="btn btn-outline-info" href="{{ route('onepages.show',$onepage->_id) }}" role="button"><i class="fa fa-lg fa-eye"  title="View"></i></a>

               <a id="edit_onepage"class="btn btn-outline-warning" href="{{ route('onepages.edit',$onepage->_id) }}" role="button"><i class="fa fa-lg fa-pencil"  title="Edit"></i></a>


              <button type="button" title="Delete" class="btn btn-outline-danger"><i class="fa fa-lg fa-trash"></i></button>
            </div>
          </td>
                </tr>
               
 @endforeach
                
              </tbody>
            </table>
          </div>
@endif

        </div>

        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    

   
@endsection
