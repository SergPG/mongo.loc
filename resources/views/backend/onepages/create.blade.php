@extends('backend.layouts.backend')

@section('content')    

<div class="container-fluid">
     
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{ url('admin/')}}">Dashboard</a>
    </li>
    <li class="breadcrumb-item">
      <a href="{{ url('admin/onepages')}}">One Pages</a>
    </li>
      <li class="breadcrumb-item active">Create One Page</li>
  </ol>
   <!-- END Breadcrumbs-->



  <!-- Example Create One Page Card-->
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i> Create One Page
    </div>
    <!-- /.card-header-->

    <div class="card-body">
    
    
  
   
          
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
  </li>
</ul>
<div class="tab-content " id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

  <div class="card mt-3">
     <!--  <div class="card-header">Register an Account</div> -->
      <div class="card-body">
        
<form action="{{ route('onepages.store') }}" method="POST">
   {{ csrf_field() }}

  <div class="form-group">
    <div class="form-row">
      <div class="col-md-12">

        <label for="nameTheme"> Name Theme</label>
        {{-- <input class="form-control" id="nameTheme" name="nameTheme" type="text" aria-describedby="nameHelp" placeholder="Enter Name theme">
 --}}

    <select class="form-control" id="nameTheme" name="nameTheme">
      <option>onepage</option>
     {{--  <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option> --}}
    </select>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="form-row">
      <div class="col-md-12">
        <label for="nameOnepage"> Name OnePage</label>
        <input class="form-control" id="nameOnepage" name="nameOnepage"  type="text" aria-describedby="nameHelp" placeholder="Enter Name OnePage">
      </div>
    </div>
  </div>

        <div class="form-group">
            
            <input type="submit" class="btn btn-primary btn-block" value="Отправить">
          </div>
          
        </form>
        
      </div>
    </div>

     
  </div>
  <div class="tab-pane fade border border-primary border-top-0" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
  <div class="tab-pane fade border border-primary border-top-0" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
</div>
 
<!-- ---------------- -->

    





  </div>
    <!-- /.card-body-->

    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    <!-- /.card-footer -->
  </div>
   <!-- /.card -->
      
</div>
<!-- /.container-fluid-->
    

   
@endsection
