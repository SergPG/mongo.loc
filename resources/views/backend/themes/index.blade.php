@extends('backend.layouts.backend')

@section('content')    

   <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tables</li>
      </ol>



      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Themes

          <div class="btn-group float-right" role="group" aria-label="Basic example">
               <a id="new_theme"class="btn btn-outline-success" href="{{ route('themes.create') }}" role="button"><i class="fa fa-lg fa-plus"  title="Add Theme"></i></a>
            
          </div>

        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Office</th>
                  <th>Age</th>
                  <th>Start date</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Office</th>
                  <th>Age</th>
                  <th>Start date</th>
                  <th>Actions</th>
                </tr>
              </tfoot>
              <tbody>

                <tr>
                  <td>Tiger Nixon</td>
                  <td>System Architect</td>
                  <td>Edinburgh</td>
                  <td>61</td>
                  <td>2011/04/25</td>
                  
          <td>
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" title="View"  class="btn btn-outline-info"><i class="fa fa-lg fa-eye"></i></button>
              <button type="button" title="Edit" class="btn btn-outline-warning"><i class="fa fa-lg fa-pencil"></i></button>
              <button type="button" title="Delete" class="btn btn-outline-danger"><i class="fa fa-lg fa-trash"></i></button>
            </div>
          </td>
                </tr>
               

                
              </tbody>
            </table>
          </div>
        </div>

        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    

   
@endsection
