@extends('themes.'.$onepage->theme.'.layouts.main')

@section('content')   

 
@if(isset($sections) && is_object($sections))
   
   @foreach($sections as $key => $section)
   <!--==========  ==========-->

       @if($section->show)

            @include('themes.'.$onepage->theme.'.design.'.$section->view_f) 

       @endif 
   <!--==========  ==========-->     
   @endforeach

@endif

@endsection
