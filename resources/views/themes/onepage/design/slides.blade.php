 
@if(isset($slides) && is_object($slides))

 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

            <div class="container">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    
                    @foreach($slides as $key => $slide)

                        <li data-target="#carousel-example-generic" data-slide-to="{{ $key }}" {!! ($key)? '' : 'class="active"'  !!} ></li>

                    @endforeach

                </ol>
            </div>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                 @foreach($slides as $key => $slide)

                <div {!! ($key)? 'class="item "' : 'class="item active"'  !!} >

                    <img class="img-responsive" src="{{ asset( $slide->image) }}" alt="Slider Image">

                    <div class="container">
                        <div class="carousel-centered">
                            <div class="margin-b-40">
                                <h1 class="carousel-title"> {{ $slide->title }} </h1>
                                <p class="color-white"> {!!$slide->description !!}</p>
                            </div>
                            <a href="{{ $slide->url }}" class="btn-theme btn-theme-sm btn-white-brd text-uppercase">Explore</a>
                        </div>
                    </div>
                </div>
                 
                @endforeach 

            </div>
        </div>
@endif        