  <div id="{{ $section->alias }}">
            <div class="bg-color-sky-light" data-auto-height="true">
                <div class="content-lg container">
                    <div class="row margin-b-40">
                        <div class="col-sm-6">
                           <h2>{{ $section->title }}</h2>
                           <p>{{ $section->description }}</p>
                        </div>
                    </div>
                    <!--// end row -->

    @if(isset($services) && is_object($services)) 


    @foreach($services as $key_srv => $service)

        @if(($key_srv)%3 == 0)
            <div class="row row-space-1 margin-b-2">
         @endif

            <div class="col-sm-4 sm-margin-b-2">
                <div class="service {!! ($service->active)? ' bg-color-base':'' !!} " data-height="height">
                    <div class="service-element">
                        <i class="service-icon  {!! $service->icon !!} {!! ($service->active)? ' color-white':'' !!} "></i>
                    </div>
                    <div class="service-info">
                        <h3 {!! ($service->active)? ' class="color-white"':'' !!} >{{$service->title}}</h3>
                        <p class="margin-b-5 {!! ($service->active)? ' color-white':'' !!}">{$service->description}}</p>
                    </div>
                        <a href="#" class="content-wrapper-link"></a>    
                </div>
            </div>



         @if(($key_srv+1)%3 == 0)
            </div>
             <!--// end row -->
         @endif

    @endforeach

    @endif
                    
                </div>
            </div>
        </div>