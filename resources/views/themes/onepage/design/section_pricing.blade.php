<div id="pricing">
            <div class="bg-color-sky-light">
                <div class="content-lg container">

    @if(isset($pricings) && is_object($pricings))

       @foreach($pricings as $key_prc => $pricing)
         
         @if(($key_prc)%3 == 0)
            <div class="row row-space-1">
         @endif

             <div class="col-sm-4 sm-margin-b-2">
                <!-- Pricing -->
                <div class="pricing {!!($pricing->active)? ' pricing-active': '' !!} ">
                    <div class="margin-b-30">
                        <i class="pricing-icon {!! $pricing->icon !!}"></i>
                        <h3> {{ $pricing->title }} <span> - $</span> {{ $pricing->price }}</h3>
                        <p>{{ $pricing->description }}</p>
                    </div>
                        @if(is_array($pricing->data_sheet) )
                            <ul class="list-unstyled pricing-list margin-b-50">
                                @for($i = 0; $i < count($pricing->data_sheet); $i++)
                                  <li class="pricing-list-item">
                                      {{ $pricing->data_sheet[$i] }}
                                  </li>
                                @endfor
                            </ul>
                        @endif
                            <a href="pricing.html" class="btn-theme btn-theme-sm btn-default-bg text-uppercase">Choose</a>
                </div>
                <!-- End Pricing -->
            </div>

         @if(($key_prc+1)%3 == 0)
            </div>
             <!--// end row -->
         @endif

       @endforeach

    @endif

                </div>
            </div>
        </div>