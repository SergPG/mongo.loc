<footer class="footer">

         @if(isset($menuLists) && is_object($menuLists))

         
            <!-- Links -->
            <div class="section-seperator">
                <div class="content-md container">
                    <div class="row">
                        <div class="col-sm-2 sm-margin-b-30">

                           @if(isset($menuLists->menu_footer_onepage))

                            <!-- List -->
                            <ul class="list-unstyled footer-list">
                                @foreach($menuLists->menu_footer_onepage as $item_name => $item_link)
                                <li class="footer-list-item"><a href="{{$item_link}}">{{$item_name }}</a></li>
                                
                                @endforeach
                            </ul>
                            <!-- End List -->
                            @endif

                        </div>

                        <div class="col-sm-2 sm-margin-b-30">

                            @if(isset($menuLists->menu_social_onepage))
                            <!-- List -->
                            <ul class="list-unstyled footer-list">

                                @foreach($menuLists->menu_social_onepage as $item_name => $item_link)
                                <li class="footer-list-item"><a href="{{$item_link}}">{{$item_name }}</a></li>
                                @endforeach

                            </ul>
                            <!-- End List -->
                            @endif
                        </div>
                        <div class="col-sm-3">

                            @if(isset($menuLists->menu_contracts_onepage))
                            <!-- List -->
                            <ul class="list-unstyled footer-list">

                                @foreach($menuLists->menu_contracts_onepage as $item_name => $item_link)
                                <li class="footer-list-item"><a href="{{$item_link}}">{{$item_name }}</a></li>
                                @endforeach
                                
                            </ul>
                            <!-- End List -->
                            @endif

                        </div>
                    </div>
                    <!--// end row -->
                </div>
            </div>
            <!-- End Links -->

          
         @endif   

            <!-- Copyright -->
            <div class="content container">
                <div class="row">
                    <div class="col-xs-6">
                        <img class="footer-logo" src="{{ asset( $onepage->logo['footer-logo']) }}" alt="Aitonepage Logo">
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="margin-b-0"><a class="fweight-700" href="http://keenthemes.com/preview/aitonepage/">Aitonepage</a> Theme Powered by: <a class="fweight-700" href="http://www.keenthemes.com/">KeenThemes.com</a></p>
                    </div>
                </div>
                <!--// end row -->
            </div>
            <!-- End Copyright -->
        </footer>