  <div id="{{ $section->alias }}">
            <div class="section-seperator">
                <div class="content-md container">
                    <div class="row margin-b-40">
                        <div class="col-sm-6">
                           <h2>{{ $section->title }}</h2>
                           <p>{{ $section->description }}</p>
                        </div>
                    </div>
                    <!--// end row -->

<!-- Masonry Grid -->
<div class="masonry-grid row">
    <div class="masonry-grid-sizer col-xs-6 col-sm-6 col-md-1"></div>

 @if(isset($works) && is_object($works))

    @foreach($works as $key_wrk => $work)

     <div class="masonry-grid-item col-xs-12 col-sm-6  {!! ($key_wrk%5 == 0)? 'col-md-8':'col-md-4'  !!}  col-md-8 margin-b-30">
        <!-- Work -->
        <div class="work work-popup-trigger">
            <div class="work-overlay">
                <img class="full-width img-responsive" src="{{ asset( $work->image) }}" alt="Portfolio Image">
            </div>

            <div class="work-popup-overlay">
                <div class="work-popup-content">
                    <a href="javascript:void(0);" class="work-popup-close">Hide</a>
                    <div class="margin-b-30">
                        <h3 class="margin-b-5">{{$work->title}}</h3>
                        <span>{!! $work->category  !!}</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 work-popup-content-divider sm-margin-b-20">
                            <div class="margin-t-10 sm-margin-t-0">
                                {!!  $work->text !!}

                                 @if(isset($work->tegs))
                                 <ul class="list-inline work-popup-tag">
                                    @foreach($work->tegs as $item_name => $item_link)
                                         <li class="work-popup-tag-item"><a class="work-popup-tag-link" href="{{ asset( $item_link) }}">{{ $item_name }} </a></li>
                                    @endforeach                
                                 </ul>
                                 @endif
                            </div>   
                        </div>
                        <div class="col-sm-4">
                            @if(isset($work->workgroup))
                            <div class="margin-t-10 sm-margin-t-0">
                              @foreach($work->workgroup as $item_name => $item_user)
                                <p class="margin-b-5"><strong> {{$item_name}}:</strong> {{ $item_user}}</p>
                              @endforeach  
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End Work -->
    </div>

    @endforeach
 
@endif
<!-- End Work -->

    </div>
    <!-- End Masonry Grid -->
    </div>
</div>
            

@if(isset($swipings) && is_object($swipings))

<!--   -----    Clients  === -->

     @foreach($swipings as $key_swp => $swiping)

     @if($swiping->alias == 'clients')

            <div class="content-lg container">
                <!-- Swiper Clients -->
                <div class="swiper-slider swiper-clients">

                    <!-- Swiper Wrapper -->
                    <div class="swiper-wrapper">
                    
                    @if(isset($swiping->clients))
                        
                    @foreach($swiping->clients as $name_client => $logo_client)

                        <div class="swiper-slide">
                            <img class="swiper-clients-img" src="{{ asset($logo_client)}}" alt="{{ $name_client }}" title="{{$name_client}}">
                        </div>
                    @endforeach

                        
                    @endif

                    </div>
                    <!-- End Swiper Wrapper -->
                </div>
                <!-- End Swiper Clients -->
            </div>
            <!-- End Clients -->
           @endif 

         @endforeach   
      @endif      
    </div>