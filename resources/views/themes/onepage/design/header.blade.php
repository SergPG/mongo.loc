<header class="header navbar-fixed-top">
            <!-- Navbar -->
            <nav class="navbar" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container js_nav-item">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon"></span>
                        </button>

                        <!-- Logo     themes.'.$theme.' -->
                        <div class="logo">
                            <a class="logo-wrap" href="#body">
                                <img class="logo-img logo-img-main" src="{{ asset( $onepage->logo['logo-img-main']) }}" alt="Asentus Logo">
                                <img class="logo-img logo-img-active" src="{{ asset( $onepage->logo['logo-img-active']) }}" alt="Asentus Logo">
                            </a>
                        </div>
                        <!-- End Logo -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="menu-container">
                            <ul class="nav navbar-nav navbar-nav-right">
                                
								<li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="{{url('/')}}">Back to ...</a></li>

                        @if(isset($menuLists->menu_top_onepage))

                                 @foreach($menuLists->menu_top_onepage as $item_name => $item_link)

                                 <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="{{$item_link}}">{{$item_name }}</a></li>

                                @endforeach

                               
                               

                              @endif    

                            </ul>
                        </div>
                    </div>
                    <!-- End Navbar Collapse -->
                </div>
            </nav>
            <!-- Navbar -->
        </header>