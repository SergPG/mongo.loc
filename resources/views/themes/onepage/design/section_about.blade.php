 <div id="{{ $section->alias }}">

            <div class="content-lg container">
            @if(isset($notes) && is_object($notes))  
                <!-- Masonry Grid -->
                <div class="masonry-grid row">

                    <div class="masonry-grid-sizer col-xs-6 col-sm-6 col-md-1"></div>

                @foreach($notes as $key_nt => $note)

                    @if(($key_nt % 2) == 0 )

                    <div class="masonry-grid-item col-xs-12 col-sm-6 col-md-4 {!! ($key_nt)? '' : 'sm-margin-b-30'  !!} ">

                        <div class="margin-b-60 ">
                            <h2>{{ $note->title }}</h2>
                            <p>{{ $note->description }}</p>
                        </div>
                        <img class="full-width img-responsive wow fadeInUp" src="{{ asset($note->image) }}" alt="Portfolio Image" data-wow-duration=".3" data-wow-delay=".{{ 2+$key_nt}}s">
                    </div>

                    @else
                       
                       <div class="masonry-grid-item col-xs-12 col-sm-6 col-md-4">

                        <div class="margin-b-60 ">
                            <img class="full-width img-responsive wow fadeInUp" src="{{ asset($note->image) }}" alt="Portfolio Image" data-wow-duration=".3" data-wow-delay=".{{ 2+$key_nt}}s">
                        </div>
                        <h2>{{ $note->title }}</h2>
                        <p>{{ $note->description }}</p>
                    </div>

                    @endif

                @endforeach                    

                </div>
                <!-- End Masonry Grid -->
            @endif
            </div>
           
<!-- ==============    =============  -->
         
            <div class="bg-color-sky-light">
                <div class="content-lg container">
                    <div class="row">

                    @if(isset($oneposts) && is_object($oneposts)) 
    
                        <div class="col-md-5 col-sm-5 md-margin-b-60">

                          @foreach($oneposts as $key_onepst => $onepost)

                            <div class="margin-t-50 margin-b-30">
                                <h2>{{ $onepost->title }}</h2>
                                <p>{{ $onepost->description }}</p>
                            </div>
                            <a href="#" class="btn-theme btn-theme-sm btn-white-bg text-uppercase">Explore</a>

                          @endforeach 

                        </div>
                    @endif

<!-- ==============    =============  -->

                    
 @if(isset($accordions) && is_object($accordions)) 

 <div class="col-md-5 col-sm-7 col-md-offset-2">

 <!-- Accordion -->
 <div class="accordion">

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        @foreach($accordions as $key_acc => $accordion)

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{$key_acc}}">
                <h4 class="panel-title">
                    <a class="panel-title-child" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key_acc}}" aria-expanded="{!! ($key_acc)?'false':'true'!!}" aria-controls="collapseOne">
                        {{$accordion->title}}
                    </a>
                </h4>
            </div>

    <div id="collapse{{$key_acc}}" class="panel-collapse collapse {!! ($key_acc)?'':'in'!!}" role="tabpanel" aria-labelledby="heading{{$key_acc}}">
                <div class="panel-body">
                    {{$accordion->text}}
                </div>
            </div>
        </div>

        @endforeach
                            
    </div>
  </div>
  <!-- End Accodrion -->
 </div>
 @endif    

    </div>
    <!--// end row -->
   </div>
  </div>
</div>
<!-- end # About -->