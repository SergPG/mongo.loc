<div id="contact">
            <!-- Contact List -->
            <div class="section-seperator">
                <div class="content-lg container">

    @if(isset($contacts) && is_object($contacts))  
       @foreach($contacts as $key_cnt => $contact)

            @if(($key_cnt)%3 == 0)
                 <div class="row">
            @endif
                    
                        <!-- Contact List -->
                        <div class="col-sm-4 sm-margin-b-50">
                            <h3><a href="#">{{$contact->city}}</a> <span class="text-uppercase margin-l-20">{{$contact->office}}</span></h3>
                            <p>{{$contact->description}}</p>    
                            
                         @if(isset($contact->contact_list) && !empty($contact->contact_list))
                            <ul class="list-unstyled contact-list">
                                 @foreach($contact->contact_list as $item_icon => $item_value)
                                                                           
                                        @for($i = 0; $i < count($item_value); $i++)
                                        <li>  
                                            <i class="margin-r-10 color-base {!! $item_icon !!}"></i>
                                            {{ $item_value[$i] }}
                                        </li>
                                        @endfor
                               
                                 @endforeach
      
                            </ul>
                        @endif

                        </div>
                        <!-- End Contact List -->        

            @if(($key_cnt+1)%3 == 0)
                </div>
                 <!--// end row -->
            @endif
      @endforeach
    @endif

                </div>
            </div>
            <!-- End Contact List -->
            
            <!-- Google Map -->
            <div id="map" class="map height-300"></div>
        </div>