<div id="products">
            <div class="content-lg container">
                <div class="row margin-b-40">
                    <div class="col-sm-6">
                        <h2>{{ $section->title }}</h2>
                        <p>{{ $section->description }}</p>
                    </div>
                </div>
                <!--// end row -->

  @if(isset($products) && is_object($products))

     @foreach($products as $key_prd => $product)

         @if(($key_prd)%3 == 0)
            <div class="row ">
         @endif

       

         <!-- Latest Products -->
            <div class="col-sm-4 sm-margin-b-50">
                <div class="margin-b-20">
                    <img class="img-responsive" src="{{ asset( $product->image) }}" alt="Latest Products Image">
                </div>
                <h4><a href="#"> {{ $product->title }}</a> <span class="text-uppercase margin-l-20">{{ $product->category }}</span></h4>
                <p>{{ $product->description }}</p>
                <a class="link" href="#">Read More</a>
            </div>
        <!-- End Latest Products -->

       
                
        @if(($key_prd+1)%3 == 0)
            </div>
             <!--// end row -->
         @endif

    @endforeach

  @endif

            </div>
        </div>