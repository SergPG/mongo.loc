<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

//use Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $exists = Storage::disk('images')->exists('02.jpg');
      //$furl = Storage::disk('themes')->get('02.jpg');
   //    $url = Storage::url('themes/22/02.jpg');

       
      $url = Storage::url('images/1920x1080/02.jpg');
      // $url = 'https://picsum.photos/1920/1080';
       //D:\ospanel\domains\mongo.loc\storage\app\public\images\1920x1080

       // $ppt = public_path('images');
       // $ppt2 = storage_path('app/public/images');

      // Storage::copy('old/file1.jpg', 'new/file1.jpg');
       // Storage::disk('s3')->put('avatars/1', $fileContents);

       //$ff = Storage::disk('images')->put('22','002.jpg');

       //dd($exists, $url, $ppt, $ppt2 );

       // $contents = Storage::get('theme.txt');

        //$url = Storage::url('theme.txt');
     
        // dd($url, $contents);

          // $sections = config('themes.onepage.sections');
          // $components = config('themes.onepage.components');

         //dd($sections);


        return view('home')->with([
                                    'url'=> $url,
                                    ]);
    }
}
