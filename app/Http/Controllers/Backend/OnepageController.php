<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Onepage;

use App\Models\Section;
use App\Models\MenuList;
use App\Models\Slide;
use App\Models\Note;
use App\Models\Onepost;
use App\Models\Accordion;
use App\Models\Product;
use App\Models\Pricing;
use App\Models\Work;
use App\Models\Swiping;
use App\Models\Service;
use App\Models\Contact;


class OnepageController extends Controller
{
  protected $vars = array();
   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       $onepages = Onepage::all();
        //dd($onepages);

        return view('backend.onepages.index')->with([
                                    'onepages'=> $onepages,
                                    ]);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.onepages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $input = $request->all();

        $theme = $request->nameTheme;
        $site_name = $request->nameOnepage;

//-----------------------------------------
        $config_site = config('themes.'.$theme.'.site');

       //dd($config_site);

        $onepage = new Onepage;
            
             $onepage->name = $site_name;
             $onepage->theme = $theme;

             foreach ($config_site as $name => $value) {
                    $onepage->$name = $value;
             }
        $onepage->save();


//----------------    !!!!!!!!!!!!!!!    ------------------------
        $config_section = config('themes.'.$theme.'.sections');
        $position = 1;
        
             foreach ($config_section as $key => $item) {
                $section = new Section;
                 $section->position = $position++; 
                   foreach ($item as $name => $value) {
                         $section->$name = $value;                      
                    }
                $onepage->sections()->save($section);
             }
        
//----------------    !!!!!!!!!!!!!!!    ------------------------
        $config_list = config('themes.'.$theme.'.menuLists');
       
        foreach ($config_list as $key => $item) {
            $menuList = new MenuList;
                 foreach ($item as $name => $value) {
                    $menuList->$name = $value;
                 }
            $onepage->menuLists()->save($menuList);
        }
//------------------------------------------
        $config_slide = config('themes.'.$theme.'.slides');

        foreach ($config_slide as $key => $item) {
            $slide = new Slide;
                  foreach ($item as $name => $value) {
                       $slide->$name = $value;
                    }
            $onepage->slides()->save($slide);
         }   
//------------------------------------------
        $config_note = config('themes.'.$theme.'.notes');

        foreach ($config_note as $key => $item) {
             $note = new Note;
                 foreach ($item as $name => $value) {
                    $note->$name = $value;
                 }
            $onepage->notes()->save($note);
        }
//------------------------------------------
        $config_onepost = config('themes.'.$theme.'.oneposts');

        foreach ($config_onepost as $key => $item) {
            $onepost = new Onepost;
                 foreach ($item as $name => $value) {
                    $onepost->$name = $value;
                }
            $onepage->oneposts()->save($onepost);
         }
//------------------------------------------
        $config_accordion = config('themes.'.$theme.'.accordions');

        foreach ($config_accordion as $key => $item) {
            $accordion = new Accordion;
                foreach ($item as $name => $value) {
                       $accordion->$name = $value;
                }
            $onepage->accordions()->save($accordion);
         }
//------------------------------------------
        $config_product = config('themes.'.$theme.'.products');

        foreach ($config_product as $key => $item) {
             $product = new Product;
                foreach ($item as $name => $value) {
                    $product->$name = $value;
                }
            $onepage->products()->save($product);
        }
//------------------------------------------
        $config_pricing = config('themes.'.$theme.'.pricings');

        foreach ($config_pricing as $key => $item) {
            $pricing = new Pricing;
                foreach ($item as $name => $value) {
                    $pricing->$name = $value;
                }
            $onepage->pricings()->save($pricing);
         }
//------------------------------------------
        $config_work = config('themes.'.$theme.'.works');

        foreach ($config_work as $key => $item) {
            $work = new Work;
                foreach ($item as $name => $value) {
                    $work->$name = $value;
                }
            $onepage->works()->save($work);
         }
//------------------------------------------
        $config_swiping = config('themes.'.$theme.'.swipings');

        foreach ($config_swiping as $key => $item) {
            $swiping = new Swiping;
                foreach ($item as $name => $value) {
                    $swiping->$name = $value;
                }
            $onepage->swipings()->save($swiping);
         }
//------------------------------------------
        $config_service = config('themes.'.$theme.'.services');

        foreach ($config_service as $key => $item) {
            $service = new Service;
                foreach ($item as $name => $value) {
                    $service->$name = $value;
                }
            $onepage->services()->save($service);
         }
//------------------------------------------
        $config_contact = config('themes.'.$theme.'.contacts');

        foreach ($config_contact as $key => $item) {
            $contact = new Contact;
                foreach ($item as $name => $value) {
                    $contact->$name = $value;
                }
            $onepage->contacts()->save($contact);
         }
//-----------------------------------------------------------

        


echo "string";
   //  return redirect()->route('profile', [$user]);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $onepage = Onepage::find($id);

        $this->vars = array_add($this->vars,'onepage',$onepage);  

        $sections = $onepage->sections->sortBy('position');
        if (!empty($sections)) {
             $this->vars = array_add($this->vars,'sections',$sections);             
        }
       
        $menuLists = $onepage->menuLists;
        if (!empty($menuLists)) {
             $this->vars = array_add($this->vars,'menuLists',$menuLists);             
        }
       

        $slides = $onepage->slides;
        if (!empty($slides)) {
             $this->vars = array_add($this->vars,'slides',$slides);             
        }

        $notes = $onepage->notes;
         if (!empty($notes)) {
             $this->vars = array_add($this->vars,'notes',$notes);             
        }

        $oneposts = $onepage->oneposts;
         if (!empty($oneposts)) {
             $this->vars = array_add($this->vars,'oneposts',$oneposts);             
        }

        $accordions = $onepage->accordions;
        if (!empty($accordions)) {
             $this->vars = array_add($this->vars,'accordions',$accordions);             
        }

        $products = $onepage->products;
         if (!empty($products)) {
             $this->vars = array_add($this->vars,'products',$products);             
        }

        $pricings = $onepage->pricings;
        if (!empty($pricings)) {
             $this->vars = array_add($this->vars,'pricings',$pricings);             
        }

        $works = $onepage->works;
         if (!empty($works)) {
             $this->vars = array_add($this->vars,'works',$works);             
        }

        $swipings = $onepage->swipings;
         if (!empty($swipings)) {
             $this->vars = array_add($this->vars,'swipings',$swipings);             
        }

        $services = $onepage->services;
         if (!empty($services)) {
             $this->vars = array_add($this->vars,'services',$services);             
        }
            
         $contacts = $onepage->contacts;
         if (!empty($contacts)) {
             $this->vars = array_add($this->vars,'contacts',$contacts);             
        }
            

       // dd($sections);

       // echo ($onepage->name);
        return view('themes.'.$onepage->theme.'.index')->with($this->vars );


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('backend.onepages.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
