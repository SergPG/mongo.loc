<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Service extends Eloquent
{
    //
	  protected $collection = 'service';

	public function onepage()
    {
        return $this->belongsTo('App\Models\Onepage','id_onepage','_id');
    }

     


}
