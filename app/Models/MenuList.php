<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MenuList extends Eloquent
{
    //
	protected $collection = 'menuLists';

	public function onepage()
    {
        return $this->belongsTo('App\Models\Onepage','id_onepage','_id');
    }
}
