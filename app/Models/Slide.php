<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Slide extends Eloquent
{
    //
	protected $collection = 'slides';

	public function onepage()
    {
        return $this->belongsTo('App\Models\Onepage','id_onepage','_id');
    }
}
