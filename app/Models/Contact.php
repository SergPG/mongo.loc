<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Contact extends Eloquent
{
    //
	  protected $collection = 'contacts';

	public function onepage()
    {
        return $this->belongsTo('App\Models\Onepage','id_onepage','_id');
    }


}
