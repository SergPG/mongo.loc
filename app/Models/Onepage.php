<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Onepage extends Eloquent
{
    //
	  protected $collection = 'onepages';

	  public function sections()
    {
        return $this->hasMany('App\Models\Section','id_onepage','_id');
    } 

    public function menuLists()
    {
        return $this->hasOne('App\Models\MenuList','id_onepage','_id');
    }

    public function slides()
    {
        return $this->hasMany('App\Models\Slide','id_onepage','_id');
    }

 		public function notes()
    {
        return $this->hasMany('App\Models\Note','id_onepage','_id');
    }

		public function oneposts()
    {
        return $this->hasMany('App\Models\Onepost','id_onepage','_id');
    }

    public function accordions()
    {
        return $this->hasMany('App\Models\Accordion','id_onepage','_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','id_onepage','_id');
    }

    public function pricings()
    {
        return $this->hasMany('App\Models\Pricing','id_onepage','_id');
    }

    public function works()
    {
        return $this->hasMany('App\Models\Work','id_onepage','_id');
    }

    public function swipings()
    {
        return $this->hasMany('App\Models\Swiping','id_onepage','_id');
    }

    public function services()
    {
        return $this->hasMany('App\Models\Service','id_onepage','_id');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact','id_onepage','_id');
    }


}
