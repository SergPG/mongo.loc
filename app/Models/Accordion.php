<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Accordion extends Eloquent
{
    //
	protected $collection = 'accordion';

	public function onepage()
    {
        return $this->belongsTo('App\Models\Accordion','id_onepage','_id');
    }
}
