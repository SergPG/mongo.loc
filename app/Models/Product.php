<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Product extends Eloquent
{
    //
	  protected $collection = 'products';

	public function onepage()
    {
        return $this->belongsTo('App\Models\Onepage','id_onepage','_id');
    }

     


}
